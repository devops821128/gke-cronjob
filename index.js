const { MongoClient, ServerApiVersion } = require("mongodb");
const uri = process.env.MONGODB_URL;

const data2021 = require("./mock/2021.json");
const data2022 = require("./mock/2022.json");
const data2023 = require("./mock/2023.json");

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

async function run() {
  try {
    // Connect the client to the server	(optional starting in v4.7)
    await client.connect();
    // Send a ping to confirm a successful connection
    await client.db("admin").command({ ping: 1 });
    console.log("Pinged your deployment. You successfully connected to MongoDB!");

    // Database Name
    const dbName = "cronJobTest";
    const db = client.db(dbName);

    const thisYear = new Date().getFullYear();
    const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const thisMonth = month[new Date().getMonth()];
    const today = new Date().getDate() > 9 ? new Date().getDate() : "0" + new Date().getDate();
    const thisHour = new Date().getHours() > 9 ? new Date().getHours() : "0" + new Date().getHours();
    const thisMinute = new Date().getMinutes() > 9 ? new Date().getMinutes() : "0" + new Date().getMinutes();
    const thisSecond = new Date().getSeconds() > 9 ? new Date().getSeconds() : "0" + new Date().getSeconds();
    const thisMilliSeconds = new Date().getMilliseconds();

    const nowString = `${thisYear}_${thisMonth}_${today}_${thisHour}_${thisMinute}_${thisSecond}_${thisMilliSeconds}`;

    const collection = db.collection(nowString);
    const finalData = [...data2021, ...data2022, ...data2023];
    console.log("finalData.length: ");
    console.log(finalData.length);
    const insertResult = await collection.insertMany(finalData);
    console.log("Inserted documents =>", JSON.stringify(insertResult));

    setTimeout(() => {
      console.log("Delayed for 1 second.");
    }, 120000);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);
