# GKE CronJob 練習 + 學習

### 相關資源

[Kubernetes: Cron Jobs](https://medium.com/google-cloud/kubernetes-cron-jobs-455fdc32e81a)

### Crontab 的語法解釋工具

[crontab guru](https://crontab.guru/)

##### 建立或是更新檔案 deployment

```
kubectl apply -f gke-resource/cronjob.yaml
```

##### 建立或是更新檔案 secret

```
kubectl apply -f gke/secret.yaml
```

##### 更新 images 指令

```
kubectl set image cronjob/hello cronjob-test=eugeneyiew128/gke-practice:latest
```

##### 檢視 pod

```
kubectl get pod
```

##### 檢查 pod error logs

```
kubectl logs <pod_name>
```

##### 刪除失敗的 Job 紀錄

```
kubectl delete job <job-name>
```

##### 刪除 Cron Job

```
kubectl delete cronjob <cronjob-name>
```

##### docker images 推到 Docker Hub 上

###### 環境: dev, qa, it, staging, prod

```
sh build.sh <環境名稱> <日期+版號>
```

##### helm 驗證

```
helm lint app-helm/
```

##### helm 起服務

```
helm install <服務名稱> app-helm
```

##### 更新 Pod image 和 tag

```
helm upgrade --reuse-values --set image.repository=<image url> --set image.tag=<image tag> <deployment name> <helm folder>

ex: helm upgrade --reuse-values --set job.image.repository=eugeneyiew128/gke-practice:cronjob4ede4a87 <服務名稱> app-helm
```

##### 列出 helm 在線上的服務

```
helm ls
```

##### 刪除 helm list 中的內容

```
helm delete gke-test-f2e
or
helm uninstall test-cron-job
```
